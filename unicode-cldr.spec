Name:           unicode-cldr
Summary:        Data from Unicode CLDR
Version:        32.0.1
Release:        1%{?dist}
License:        Unicode

URL:            http://cldr.unicode.org
Source0:        https://unicode.org/Public/cldr/%{version}/cldr-common-%{version}.zip
Source1:        https://unicode.org/Public/cldr/%{version}/cldr-keyboards-%{version}.zip

BuildArch:      noarch

BuildRequires:  unzip


%description
Unicode Common Locale Data Repository (CLDR) provides key building blocks for
software to support the world's languages, with the largest and most extensive
standard repository of locale data available. This data is used by a wide
spectrum of companies for their software internationalization and localization,
adapting software to the conventions of different languages for such common
software tasks.

CLDR uses the XML format provided by UTS #35: Unicode Locale Data Markup
Language (LDML). LDML is a format used not only for CLDR, but also for general
interchange of locale data, such as in Microsoft's .NET.


%package common
Summary:        Common data from Unicode CLDR (core)
# Provide the name used by Debian/Ubuntu too...
Provides:       %{name}-core = %{version}-%{release}

%description common
Unicode Common Locale Data Repository (CLDR) provides key building blocks for
software to support the world's languages, with the largest and most extensive
standard repository of locale data available. This data is used by a wide
spectrum of companies for their software internationalization and localization,
adapting software to the conventions of different languages for such common
software tasks.

CLDR uses the XML format provided by UTS #35: Unicode Locale Data Markup
Language (LDML). LDML is a format used not only for CLDR, but also for general
interchange of locale data, such as in Microsoft's .NET.

This package provides the core of the upstream CLDR data under the
%{_datadir}/unicode/cldr/common directory.


%package keyboards
Summary:        Keyboard data from Unicode CLDR

%description keyboards
Unicode Common Locale Data Repository (CLDR) provides key building blocks for
software to support the world's languages, with the largest and most extensive
standard repository of locale data available. This data is used by a wide
spectrum of companies for their software internationalization and localization,
adapting software to the conventions of different languages for such common
software tasks.

CLDR uses the XML format provided by UTS #35: Unicode Locale Data Markup
Language (LDML). LDML is a format used not only for CLDR, but also for general
interchange of locale data, such as in Microsoft's .NET.

This package provides the keyboard layout data from the CLDR data under the
%{_datadir}/unicode/cldr/keyboards directory.

%prep
%setup -q -c
# These files are duplicated in each zip, and unzip prompts on replacing files...
rm apache-license.txt readme.html unicode-license.txt ICU-LICENSE
%setup -q -c -D -T -a 1

%build
# Nothing to build

%install
mkdir -p %{buildroot}/%{_datadir}/unicode/cldr
cp -R common %{buildroot}/%{_datadir}/unicode/cldr
cp -R keyboards %{buildroot}/%{_datadir}/unicode/cldr

%files common
%doc readme.html
%license unicode-license.txt
%{_datadir}/unicode/cldr/common

%dir %{_datadir}/unicode

%files keyboards
%doc readme.html
%license unicode-license.txt
%{_datadir}/unicode/cldr/keyboards

%dir %{_datadir}/unicode


%changelog
* Sat Feb 24 2018 Corentin Noël <corentin@elementary.io> - 32.0.1-1
- Initial release
